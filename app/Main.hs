{-# LANGUAGE OverloadedLists #-}

module Main where

import RIO
import RIO.App qualified as RIO (App)

import Codec.Picture qualified as JP
import Codec.Picture.Types qualified as JP
import Foreign

import Control.Monad.Trans.Resource qualified as Resource
import Data.Tagged (Tagged(..))
import Engine.Setup qualified as Setup
import Engine.Types.Options (Options(..))
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types qualified as Vulkan
import Geomancy (vec3)
import Geomancy.Transform qualified as Transform
import Geomancy.Vulkan.Projection qualified as Projection
import Geomancy.Vulkan.View qualified as View
import Geometry.Cube qualified as Cube
import Render.Draw qualified as Draw
import Render.Pass (usePass)
import Render.Pass qualified as RenderPass
import Render.Pass.Offscreen qualified as Offscreen
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (withPools, oneshot_)
import Resource.Image qualified as Image
import Resource.Model qualified as Model
import Resource.Region qualified as Region
import RIO.App (appMain)
import RIO.ByteString.Lazy qualified as BSL
import RIO.Vector.Partial as Vector (headM)
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Zero (zero)
import VulkanMemoryAllocator qualified as VMA

-- keid-render-basic
import Render.DescSets.Set0 qualified as Scene
import Render.Samplers qualified as Samplers
import Render.Unlit.Colored.Pipeline qualified as UnlitColored

main :: IO ()
main = appMain (pure opts) optionsVerbose setup app
  where
    opts = Options
      { optionsVerbose = True
      , optionsFullscreen = False
      , optionsDisplay = 0
      , optionsRecyclerWait = Nothing
      , optionsMaxFPS = Nothing
      , optionsPresent = Nothing
      , optionsMsaa = Vk.SAMPLE_COUNT_8_BIT
      , optionsSize = Nothing
      }

    setup opts_ = do
      handles <- Setup.setupHeadless opts_
      pure (handles, ())

app :: RIO (RIO.App Setup.Headless ()) ()
app = Region.eval do
  let
    msaa = Vk.SAMPLE_COUNT_1_BIT
    maxAnisotropy = 16

    Vk.Extent2D{width, height} = Offscreen.sExtent offscreenSettings
    scene = Scene.emptyScene
      { Scene.sceneProjection =
          Projection.perspective
            (90/180*pi)
            (1/2048)
            16384
            width
            height
      , Scene.sceneView =
          View.lookAt
            (vec3 3 (-5) (-3))
            (vec3 0 (-2) 0)
            (vec3 0 (-1) 0)
      }

  rp <- Offscreen.allocate offscreenSettings

  samplers <- Samplers.allocate maxAnisotropy
  let sceneBinds = Scene.mkBindings samplers Nothing Nothing 0
  pWireframe <- UnlitColored.allocateWireframe True msaa sceneBinds rp

  context <- ask
  (outImage, outAllocation, outInfo) <- VMA.createImage
    (Vulkan.getAllocator context)
    ( outImageCI
        (Offscreen.sFormat offscreenSettings)
        (Offscreen.sExtent offscreenSettings)
    )
    outImageAllocationCI
  RIO.void $! Resource.register $
    VMA.destroyImage
      (Vulkan.getAllocator context)
      outImage
      outAllocation

  withPools \pools -> do
    instances <-
      Buffer.createStaged
        (Just "instances")
        pools
        Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
        1
        ( Storable.fromList do
            x <- [0, 0.05 .. 2*pi]
            pure $
              Transform.translate 0 0 x <>
              Transform.rotateX x
        )
    Buffer.register instances

    bbWire <- Model.createStagedL (Just "bbWire") pools Cube.bbWireColored Nothing
    Model.registerIndexed_ bbWire

    set0layout <- Vector.headM (unTagged $ Pipeline.pDescLayouts pWireframe)

    frScene <- Scene.allocate
      (Tagged set0layout)
      Nothing
      Nothing
      Nothing
      mempty
      Nothing
    Buffer.pokeCoherent (Scene.frBuffer frScene) 0 scene

    oneshot_ context pools Vulkan.qGraphics \cb -> do
      -- XXX: render stuff
      usePass rp 1 cb do
        RenderPass.setViewportScissor cb (Offscreen.sExtent offscreenSettings) rp

        Scene.withBoundSet0 frScene pWireframe cb do
          Graphics.bind cb pWireframe do
            Draw.indexed cb bbWire instances

      -- XXX: read out the framesbuffer image
      -- The image is already in SRC layout due to renderpass settings.
      let renderColor = Image.aiImage $ Offscreen.oColor rp

      Vk.cmdPipelineBarrier
        cb
        Vk.PIPELINE_STAGE_TOP_OF_PIPE_BIT
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        zero
        mempty
        mempty
        [ SomeStruct zero
            { Vk.srcAccessMask    = zero
            , Vk.dstAccessMask    = Vk.ACCESS_TRANSFER_WRITE_BIT
            , Vk.oldLayout        = Vk.IMAGE_LAYOUT_UNDEFINED
            , Vk.newLayout        = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
            , Vk.image            = outImage
            , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
            }
        ]

      let
        imageSubr = Vk.ImageSubresourceLayers
          { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
          , mipLevel       = 0
          , baseArrayLayer = 0
          , layerCount     = 1
          }

      Vk.cmdCopyImage
        cb
        renderColor
        Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        outImage
        Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        [ Vk.ImageCopy
            { srcSubresource =
                imageSubr
            , srcOffset =
                Vk.Offset3D 0 0 0
            , dstSubresource =
                imageSubr
            , dstOffset =
                Vk.Offset3D 0 0 0
            , extent =
                Vk.Extent3D width height 1
            }
        ]

      Vk.cmdPipelineBarrier
        cb
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        Vk.PIPELINE_STAGE_HOST_BIT
        zero
        mempty
        mempty
        [ SomeStruct zero
            { Vk.srcAccessMask    = Vk.ACCESS_TRANSFER_WRITE_BIT
            , Vk.dstAccessMask    = Vk.ACCESS_HOST_READ_BIT
            , Vk.oldLayout        = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
            , Vk.newLayout        = Vk.IMAGE_LAYOUT_GENERAL
            , Vk.image            = outImage
            , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
            }
        ]

  outLayout <- Vk.getImageSubresourceLayout
    (Vulkan.getDevice context)
    outImage
    ( Vk.ImageSubresource
        { aspectMask = Vk.IMAGE_ASPECT_COLOR_BIT
        , mipLevel   = 0
        , arrayLayer = 0
        }
    )
  logDebug $ "Output layout: " <> displayShow outLayout

  let
    pixelAddr x y = plusPtr
      (VMA.mappedData outInfo)
      (fromIntegral outLayout.offset
      + (y * fromIntegral outLayout.rowPitch)
      + (x * sizeOf (0 :: Word32))
      )

    Vk.Extent2D{width=outWidth, height=outHeight} =
      Offscreen.sExtent offscreenSettings

  liftIO do
    pixels <- JP.withImage
      (fromIntegral outWidth)
      (fromIntegral outHeight)
      ( \x y ->
          fmap
            (JP.unpackPixel @JP.PixelRGBA8)
            (peek $ pixelAddr x y)
      )
    BSL.writeFile "headless.png" $ JP.encodePng pixels

  logInfo "Everything is fine."
  where
    offscreenSettings = Offscreen.Settings
      { sLabel       = "Headless"
      , sExtent      = Vk.Extent2D 1024 1024
      , sFormat      = Vk.FORMAT_R8G8B8A8_UNORM
      , sColorLayout = Just Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
      , sDepthFormat = Vk.FORMAT_D32_SFLOAT_S8_UINT
      , sDepthLayout = Nothing
      , sLayers      = 1
      , sMultiView   = False
      , sSamples     = Vk.SAMPLE_COUNT_1_BIT
      , sMipMap      = False
      }

outImageCI :: Vk.Format -> Vk.Extent2D -> Vk.ImageCreateInfo '[]
outImageCI format Vk.Extent2D{width, height} = zero
  { Vk.imageType     = Vk.IMAGE_TYPE_2D
  , Vk.flags         = zero
  , Vk.format        = format
  , Vk.extent        = Vk.Extent3D width height 1
  , Vk.mipLevels     = 1
  , Vk.arrayLayers   = 1
  , Vk.tiling        = Vk.IMAGE_TILING_LINEAR
  , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
  , Vk.usage         = Vk.IMAGE_USAGE_TRANSFER_DST_BIT
  , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
  , Vk.samples       = Vk.SAMPLE_COUNT_1_BIT
  }

outImageAllocationCI :: VMA.AllocationCreateInfo
outImageAllocationCI = zero
  { VMA.flags =
      VMA.ALLOCATION_CREATE_MAPPED_BIT
  , VMA.usage =
      VMA.MEMORY_USAGE_GPU_TO_CPU
  }
